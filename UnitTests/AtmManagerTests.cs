﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork3;
using HomeWork3.Context;
using HomeWork3.Entities;
using HomeWork3.Enums;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace UnitTests
{
    public class AtmManagerTests
    {
        private readonly ApplicationContext _context;
        private readonly AtmManager _manager;

        public AtmManagerTests()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("Test")
                .Options;

            _manager = new AtmManager(options);
            _context = new ApplicationContext(options);
        }

        [Fact]
        public void GetUserInfo_ReceivedSuccessfully()
        {
            var currentUser = new User {Id = Guid.NewGuid(), Login = Guid.NewGuid().ToString(), Password = Guid.NewGuid().ToString() };
            
            AddUser(currentUser);

            var actualUser = _manager.GetUserInfo(currentUser.Login, currentUser.Password);

            Assert.Equal(currentUser.Id, actualUser.Id);
        }

        [Fact]
        public void GetAllUserAccount_ReceivedSuccessfully()
        {
            var currentUser = new User {Id = Guid.NewGuid()};
            var expectedAccounts = new List<Account>
            {
                new Account {Id = new Guid(), UserId = currentUser.Id},
                new Account {Id = new Guid(), UserId = currentUser.Id},
                new Account {Id = new Guid(), UserId = currentUser.Id},
                new Account {Id = new Guid(), UserId = Guid.NewGuid()},
                new Account {Id = new Guid(), UserId = Guid.NewGuid()}
            };

            AddUser(currentUser);
            AddAccounts(expectedAccounts);

            var actualAccounts = _manager.GetAccounts(currentUser);

            Assert.Equal(3, actualAccounts.Count());
        }

        [Fact]
        public void GetAccountWithHistory_ReceivedSuccessfully()
        {
            var currentUser = new User {Id = Guid.NewGuid()};
            var expectedAccounts = new List<Account>
            {
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = Guid.NewGuid()},
                new Account {Id = Guid.NewGuid(), UserId = Guid.NewGuid()}
            };
            var expectedOperations = new List<OperationHistory>
            {
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.First().Id},
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.First().Id},
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.First().Id},
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(1).Id},
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(2).Id},
                new OperationHistory {Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(3).Id}
            };

            AddUser(currentUser);
            AddAccounts(expectedAccounts);
            AddOperations(expectedOperations);

            var actualAccounts = _manager.GetAccountsWithOperations(currentUser);
            var actualOperations = actualAccounts.Sum(x => x.Operations?.Count);

            Assert.Equal(3, actualAccounts.Count());
            Assert.Equal(5, actualOperations);
        }

        [Fact]
        public void GetRefillOperations_ReceivedSuccessfully()
        {
            var currentUser = new User {Id = Guid.NewGuid()};
            var expectedAccounts = new List<Account>
            {
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = currentUser.Id},
                new Account {Id = Guid.NewGuid(), UserId = Guid.NewGuid()},
                new Account {Id = Guid.NewGuid(), UserId = Guid.NewGuid()}
            };
            var expectedOperations = new List<OperationHistory>
            {
                new OperationHistory
                {
                    Id = Guid.NewGuid(), AccountId = expectedAccounts.First().Id, OperationType = OperationType.Refill
                },
                new OperationHistory
                {
                    Id = Guid.NewGuid(), AccountId = expectedAccounts.First().Id, OperationType = OperationType.Refill
                },
                new OperationHistory
                    {Id = Guid.NewGuid(), AccountId = Guid.NewGuid(), OperationType = OperationType.Refill},
                new OperationHistory
                {
                    Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(1).Id,
                    OperationType = OperationType.Withdrawals
                },
                new OperationHistory
                {
                    Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(2).Id,
                    OperationType = OperationType.Withdrawals
                },
                new OperationHistory
                {
                    Id = Guid.NewGuid(), AccountId = expectedAccounts.ElementAt(3).Id,
                    OperationType = OperationType.Withdrawals
                }
            };

            AddUser(currentUser);
            AddAccounts(expectedAccounts);
            AddOperations(expectedOperations);

            var actualOperations = _manager.GetRefillOperations();

            Assert.Equal(2, actualOperations.Count());
        }

        [Fact]
        public void GetAffluentUser_ReceivedSuccessfully()
        {
            const double minCache = 500;
            var expectedUsers = new List<User>
            {
                new User {Id = Guid.NewGuid()},
                new User {Id = Guid.NewGuid()},
                new User {Id = Guid.NewGuid()}
            };
            var expectedAccounts = new List<Account>
            {
                new Account {Id = Guid.NewGuid(), UserId = expectedUsers.ElementAt(0).Id, Balance = -9},
                new Account {Id = Guid.NewGuid(), UserId = expectedUsers.ElementAt(0).Id, Balance = 5001},
                new Account {Id = Guid.NewGuid(), UserId = expectedUsers.ElementAt(1).Id, Balance = -30},
                new Account {Id = Guid.NewGuid(), UserId = expectedUsers.ElementAt(1).Id, Balance = 499.99999},
                new Account {Id = Guid.NewGuid(), UserId = expectedUsers.ElementAt(2).Id, Balance = 0}
            };

            AddUsers(expectedUsers);
            AddAccounts(expectedAccounts);

            var actualUsers = _manager.GetAffluentUser(minCache);

            Assert.Equal(1, actualUsers.Count());
            Assert.Equal(expectedUsers.ElementAt(0).Id, actualUsers.First().Id);
        }

        private void AddUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        private void AddUsers(IEnumerable<User> users)
        {
            _context.Users.AddRange(users);
            _context.SaveChanges();
        }

        private void AddAccounts(IEnumerable<Account> accounts)
        {
            _context.Accounts.AddRange(accounts);
            _context.SaveChanges();
        }

        private void AddOperations(IEnumerable<OperationHistory> operations)
        {
            _context.Operations.AddRange(operations);
            _context.SaveChanges();
        }
    }
}