﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeWork3.Context;
using HomeWork3.Entities;
using HomeWork3.Enums;
using Microsoft.EntityFrameworkCore;

namespace HomeWork3
{
    public class AtmManager
    {
        private readonly ApplicationContext _context;

        public AtmManager(DbContextOptions<ApplicationContext> options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }
            
            _context = new ApplicationContext(options);
        }

        public User GetUserInfo(string login, string password)
        {
            return _context.Users
                .FirstOrDefault(x => x.Login == login && x.Password == password);
        }

        public IEnumerable<Account> GetAccounts(User user)
        {
            return _context.Accounts
                .Where(x => x.UserId == user.Id)
                .ToList();
        }

        public IEnumerable<Account> GetAccountsWithOperations(User user)
        {
            return _context.Accounts
                .Where(x => x.UserId == user.Id)
                .Include(nameof(_context.Operations))
                .ToList();
        }

        public IEnumerable<Tuple<OperationHistory, User>> GetRefillOperations()
        {
            return _context.Operations
                .Where(x => x.OperationType == OperationType.Refill)
                .Select(x => new Tuple<OperationHistory, User>(x, x.Account.User))
                .ToList(); ;
        }

        public IEnumerable<User> GetAffluentUser(double minimum)
        {
            return _context.Users
                .Include(nameof(_context.Accounts))
                .Where(x => x.Accounts
                    .Any(y => y.Balance > minimum))
                .ToList();
        }
    }
}