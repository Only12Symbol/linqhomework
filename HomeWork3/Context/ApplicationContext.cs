﻿using HomeWork3.Entities;
using Microsoft.EntityFrameworkCore;

namespace HomeWork3.Context
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<OperationHistory> Operations { get; set; }
    }
}