﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using HomeWork3.Enums;

namespace HomeWork3.Entities
{
    public class OperationHistory
    {
        public Guid Id { get; set; }
        public DateTimeOffset OperationData { get; set; }
        public OperationType OperationType { get; set; }
        public double Cache { get; set; }
        public Guid AccountId { get; set; }

        [ForeignKey("AccountId")] 
        public Account Account { get; set; }
    }
}