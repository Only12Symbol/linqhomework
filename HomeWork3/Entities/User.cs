﻿using System;
using System.Collections.Generic;

namespace HomeWork3.Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public int PassportSerial { get; set; }
        public int PassportNumber { get; set; }
        public DateTimeOffset RegisterDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual List<Account> Accounts { get; set; }
    }
}