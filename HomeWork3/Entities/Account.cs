﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeWork3.Entities
{
    public class Account
    {
        public Guid Id { get; set; }
        public DateTimeOffset RegistrationDate { get; set; }
        public double Balance { get; set; }
        public Guid UserId { get; set; }

        [ForeignKey("UserId")] 
        public User User { get; set; }

        public List<OperationHistory> Operations { get; set; }
    }
}