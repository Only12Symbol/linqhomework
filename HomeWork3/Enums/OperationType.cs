﻿using System.ComponentModel;

namespace HomeWork3.Enums
{
    public enum OperationType
    {
        [Description("Пополнение")] 
        Refill = 0,

        [Description("Вывод")] 
        Withdrawals = 1
    }
}